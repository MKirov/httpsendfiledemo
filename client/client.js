var request = require("request");
var path = require("path");
var fs = require("fs");

var filename = "./unsend/DSC_3589.JPG";
var isSending = false;

function sendFile(filename) {
    if(isSending)
      return;

    isSending = true;
    var target = "http://localhost:3000/upload/" + path.basename(filename);
  
    var rs = fs.createReadStream(filename);
    var ws = request.post(target);
  
    ws.on("drain", function () {
      console.log("drain", new Date());
      rs.resume();
    });
  
    rs.on("end", function () {
      console.log("uploaded to " + target);
      isSending = false;
    });
  
    ws.on("error", function (err) {
      console.error("cannot send file to " + target + ": " + err);
      isSending = false;
    });
  
    rs.pipe(ws);
}

setInterval(() => {
    sendFile(filename)
}, 1000);